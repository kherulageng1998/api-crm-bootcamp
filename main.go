package main

import (
	"api-crm/config"
	"api-crm/controllers/admin"
	"api-crm/controllers/customer"
	"api-crm/middlewares"
	"log"

	"github.com/gin-gonic/gin"
)

func main() {
	config.InitDB()
	sqlDB, err := config.DB.DB()
	if err != nil {
		log.Fatal("Error ini: " + err.Error())
	}
	// Close
	defer sqlDB.Close()

	r := gin.Default()

	api := r.Group("/api")
	{
		auth := api.Group("/auth")
		{
			auth.POST("/login", admin.Login)
		}

		adm := api.Group("/admin", middlewares.RequireAuth)
		{
			adm.POST("/registration", admin.RegistrationAdmin)
			adm.GET("/get-list-approval", admin.GetListApproveAdmin)
			adm.PUT("/approve", admin.ApproveAdmin)
		}

		cust := api.Group("/cust", middlewares.RequireAuth)
		{
			cust.POST("/registration", customer.RegistrationCustomer)
			cust.GET("/get-list", customer.GetListCustomer)
			cust.DELETE("/delete/:id", customer.DeleteCustomer)
		}
	}

	r.Run() // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}
