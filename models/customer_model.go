package models

import "time"

type Customer struct {
	Id         int       `json:"id"`
	First_name string    `json:"first_name" binding:"required"`
	Last_name  string    `json:"last_name" binding:"required"`
	Email      string    `json:"email" binding:"required"`
	Avatar     string    `json:"avatar" binding:"required"`
	Created_at time.Time `json:"created_at,omitempty"`
	Updated_at time.Time `json:"updated_at,omitempty"`
}

func (Customer) TableName() string {
	return "customer"
}
