package models

type Admin struct {
	Id             int    `json:"id"`
	Username       string `json:"username" binding:"required"`
	Password       string `json:"password" binding:"required"`
	Role_id        int    `json:"role_id" binding:"required"`
	Super_admin_id int    `json:"super_admin_id,omitempty" binding:"required"`
	Verified       string `json:"verified" gorm:"type:enum('true', 'false')"`
	Active         string `json:"active" gorm:"type:enum('true', 'false')"`
}

func (Admin) TableName() string {
	return "actors"
}
