package middlewares

import (
	"api-crm/config"
	"api-crm/models"
	"fmt"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
)

func ExtractToken(c *gin.Context) string {
	token := c.Query("token")
	if token != "" {
		return token
	} else {
		auth := c.Request.Header.Get("Authorization")
		if auth == "" {
			c.String(http.StatusOK, "No Authorization header provided")
			c.Abort()
			return ""
		}
		token := ""
		token = strings.TrimPrefix(auth, "Bearer ")
		fmt.Println("ini token: " + token)
		return token
	}
}

func RequireAuth(c *gin.Context) {

	tokenString := ExtractToken(c)
	if tokenString == "" || tokenString == "undefined" {
		c.AbortWithStatusJSON(http.StatusUnauthorized, &models.Response{
			Status:  false,
			Message: "Unauthorized 1",
			Data:    nil,
		})
		return
	}

	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			fmt.Println("Cek 1")
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(os.Getenv("SECRET")), nil
	})

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		if float64(time.Now().Unix()) > claims["exp"].(float64) {
			fmt.Println(claims)
			c.AbortWithStatusJSON(http.StatusUnauthorized, &models.Response{
				Status:  false,
				Message: "Unauthorized 2",
				Data:    nil,
			})
			return
		}

		var user models.Admin
		var count int64 = 0
		userId := claims["sub"].(float64)
		username := claims["name"].(string)
		config.DB.Debug().Where("id = ? AND username = ?", userId, username).First(&user).Count(&count)

		if count < 1 {
			fmt.Println(count)
			fmt.Println(claims["sub"])
			c.AbortWithStatusJSON(http.StatusUnauthorized, &models.Response{
				Status:  false,
				Message: "Unauthorized 3",
				Data:    nil,
			})
			return
		}

		c.Set("user", user)
		fmt.Println(user)

		c.Next()
	} else {
		fmt.Println(err)
		c.AbortWithStatusJSON(http.StatusUnauthorized, &models.Response{
			Status:  false,
			Message: "Unauthorized 4. Token in not valid!",
			Data:    nil,
		})
		return
	}
}
