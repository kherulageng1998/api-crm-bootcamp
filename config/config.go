package config

import (
	_ "database/sql"

	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var DB *gorm.DB

func InitDB() {
	var err error
	dsn := "root:Indopride2020@tcp(127.0.0.1:3306)/crm_bootcamp?charset=utf8mb4&parseTime=True&loc=Local"
	DB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		fmt.Println("Failed to connect to database!")
	} else {
		fmt.Println("Success to connect database!")
	}
}
