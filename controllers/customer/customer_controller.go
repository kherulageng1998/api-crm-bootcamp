package customer

import (
	"api-crm/config"
	"api-crm/models"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func GetListCustomer(c *gin.Context) {
	var customer []models.Customer

	var count int64

	err := config.DB.
		Find(&customer).
		Count(&count).Error
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			c.AbortWithStatusJSON(http.StatusNotFound, &models.Response{
				Status:  false,
				Message: err.Error(),
				Data:    nil,
			})
			return
		default:
			c.AbortWithStatusJSON(http.StatusInternalServerError, &models.Response{
				Status:  false,
				Message: err.Error(),
				Data:    nil,
			})
			return
		}
	}

	if count > 0 {
		c.JSON(http.StatusOK, &models.Response{
			Status:  true,
			Message: "Get data berhasil!",
			Data:    customer,
		})
	} else {
		c.JSON(http.StatusOK, &models.Response{
			Status:  false,
			Message: "Data kosong!",
			Data:    nil,
		})
	}
}

func DeleteCustomer(c *gin.Context) {
	tx := config.DB.Begin()
	var customer models.Customer
	id := c.Param("id")
	if err := tx.Where("id=?", id).Delete(&customer).Error; err != nil {
		tx.Rollback()
		c.JSON(http.StatusBadRequest, &models.Response{
			Status:  false,
			Message: err.Error(),
			Data:    nil,
		})
	}

	tx.Commit()
	c.JSON(http.StatusOK, &models.Response{
		Status:  true,
		Message: "Delete planning berhasil!",
		Data:    nil,
	})
}
