package customer

import (
	"api-crm/config"
	"api-crm/models"
	"encoding/base64"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func RegistrationCustomer(c *gin.Context) {
	tx := config.DB.Begin()
	var customer models.Customer

	var date string = time.Now().Format("2006.01.02 15:04:05")
	dateNow, error := time.Parse("2006.01.02 15:04:05", date)
	if error != nil {
		log.Fatal(error)
	}

	if err := c.ShouldBindJSON(&customer); err != nil {
		tx.Rollback()
		c.JSON(http.StatusOK, &models.Response{
			Status:  false,
			Message: "Registration customer failed! " + err.Error(),
			Data:    nil,
		})
		return
	}

	dec, err := base64.StdEncoding.DecodeString(customer.Avatar)
	if err != nil {
		tx.Rollback()
		fmt.Println(err)
		panic(err)
	}

	direktori := "/upload/image/"
	if _, err := os.Stat(direktori); os.IsNotExist(err) {
		fmt.Printf("Direktori %s belum ada\n", direktori)
		err := os.MkdirAll("."+direktori, 0755)
		if err != nil {
			fmt.Println("Error gaes!")
			c.JSON(500, gin.H{"message": err.Error()})
			return
		}
	} else {
		fmt.Printf("Direktori %s sudah ada\n", direktori)
	}
	imagePath := direktori + customer.First_name + " " + customer.Last_name + ".jpg"
	hostPath := c.Request.Host
	f, err := os.Create("." + imagePath)
	if err != nil {
		tx.Rollback()
		fmt.Println(err)
		panic(err)
	}
	defer f.Close()

	if _, err := f.Write(dec); err != nil {
		tx.Rollback()
		fmt.Println(err)
		panic(err)
	}
	if err := f.Sync(); err != nil {
		tx.Rollback()
		fmt.Println(err)
		panic(err)
	}

	abs, err := filepath.Abs("." + imagePath)
	if err == nil {
		fmt.Println("Absolute:", abs)
	} else {
		tx.Rollback()
		fmt.Println(err)
		log.Fatal(err)
	}
	fmt.Println("Host:", hostPath)
	fmt.Println("Url:", c.FullPath())

	customer.Created_at = dateNow
	customer.Avatar = strings.ReplaceAll(abs, "\\", "/")

	if err := tx.Select("first_name",
		"last_name",
		"email",
		"avatar",
		"created_at").
		Create(&customer).Error; err != nil {
		tx.Rollback()
		c.JSON(http.StatusOK, &models.Response{
			Status:  false,
			Message: "Registration customer failed! " + err.Error(),
			Data:    nil,
		})
	}

	tx.Commit()

	var customers []models.Customer
	var count int64

	err = config.DB.
		Where(customer).
		Find(&customers).
		Count(&count).Error
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			c.AbortWithStatusJSON(http.StatusNotFound, &models.Response{
				Status:  false,
				Message: err.Error(),
				Data:    nil,
			})
			return
		default:
			c.AbortWithStatusJSON(http.StatusInternalServerError, &models.Response{
				Status:  false,
				Message: err.Error(),
				Data:    nil,
			})
			return
		}
	}

	if count > 0 {
		c.JSON(http.StatusOK, &models.Response{
			Status:  true,
			Message: "Registration customer success!",
			Data:    customers,
		})
	} else {
		c.JSON(http.StatusOK, &models.Response{
			Status:  false,
			Message: "Data kosong!",
			Data:    nil,
		})
	}
}
