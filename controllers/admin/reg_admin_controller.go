package admin

import (
	"api-crm/config"
	"api-crm/models"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func RegistrationAdmin(c *gin.Context) {
	tx := config.DB.Begin()

	var admin models.Admin
	var count int64

	if err := c.ShouldBindJSON(&admin); err != nil {
		tx.Rollback()
		c.JSON(http.StatusOK, &models.Response{
			Status:  false,
			Message: "Registration admin failed! " + err.Error(),
			Data:    nil,
		})
		return
	}

	var adminss []models.Admin
	err := config.DB.
		Where("id=?", admin.Super_admin_id).
		Find(&adminss).
		Count(&count).Error
	if err != nil {
		tx.Rollback()
		c.JSON(http.StatusOK, &models.Response{
			Status:  false,
			Message: "Registration admin failed! tetottt" + err.Error(),
			Data:    nil,
		})
		return
	}

	if count < 1 {
		tx.Rollback()
		c.JSON(http.StatusOK, &models.Response{
			Status:  false,
			Message: "Registration admin failed! ",
			Data:    nil,
		})
		return
	}

	admin.Verified = "false"
	admin.Active = "false"
	if err := tx.Debug().Select("username",
		"password",
		"role_id",
		"verified",
		"active").
		Create(&admin).Error; err != nil {
		tx.Rollback()
		c.JSON(http.StatusOK, &models.Response{
			Status:  false,
			Message: "Registration admin failed! " + err.Error(),
			Data:    nil,
		})
		return
	}

	tx.Commit()

	var admins []models.Admin

	err = config.DB.
		Table("actors").
		Where("username=? AND password=? AND id=?", admin.Username, admin.Password, admin.Id).
		Find(&admins).
		Count(&count).Error
	if err != nil {
		tx.Rollback()
		switch err {
		case gorm.ErrRecordNotFound:
			c.AbortWithStatusJSON(http.StatusNotFound, &models.Response{
				Status:  false,
				Message: err.Error(),
				Data:    nil,
			})
			return
		default:
			c.AbortWithStatusJSON(http.StatusInternalServerError, &models.Response{
				Status:  false,
				Message: err.Error(),
				Data:    nil,
			})
			return
		}
	}

	if count > 0 {
		tx := config.DB.Begin()

		if err := tx.Table("admin_registration").Select("admin_id",
			"super_admin_id",
			"STATUS").
			Create(map[string]interface{}{
				"admin_id":       admins[0].Id,
				"super_admin_id": admin.Super_admin_id,
				"STATUS":         "N",
			}).Error; err != nil {
			tx.Rollback()
			c.JSON(http.StatusOK, &models.Response{
				Status:  false,
				Message: "Registration admin failed! " + err.Error(),
				Data:    nil,
			})
			return
		}

		tx.Commit()

		c.JSON(http.StatusOK, &models.Response{
			Status:  true,
			Message: "Registration admin success!",
			Data:    admins,
		})
		return
	} else {
		c.JSON(http.StatusOK, &models.Response{
			Status:  false,
			Message: "Data kosong!",
			Data:    nil,
		})
		return
	}
}

func GetListApproveAdmin(c *gin.Context) {
	var admin []models.Admin

	var count int64

	err := config.DB.
		Where("verified = ? OR active = ?", "false", "false").
		Find(&admin).
		Count(&count).Error
	if err != nil {
		switch err {
		case gorm.ErrRecordNotFound:
			c.AbortWithStatusJSON(http.StatusNotFound, &models.Response{
				Status:  false,
				Message: err.Error(),
				Data:    nil,
			})
			return
		default:
			c.AbortWithStatusJSON(http.StatusInternalServerError, &models.Response{
				Status:  false,
				Message: err.Error(),
				Data:    nil,
			})
			return
		}
	}

	if count > 0 {
		c.JSON(http.StatusOK, &models.Response{
			Status:  true,
			Message: "Get data berhasil!",
			Data:    admin,
		})
	} else {
		c.JSON(http.StatusOK, &models.Response{
			Status:  false,
			Message: "Data kosong!",
			Data:    nil,
		})
	}
}

func ApproveAdmin(c *gin.Context) {
	tx := config.DB.Begin()
	type Body struct {
		Admin_id       int `json:"admin_id"`
		Super_admin_id int `json:"super_admin_id"`
	}

	var body Body
	var admin models.Admin

	if err := c.BindJSON(&body); err != nil {
		tx.Rollback()
		c.JSON(http.StatusOK, &models.Response{
			Status:  false,
			Message: "Read body json failed! " + err.Error(),
			Data:    nil,
		})
		return
	}

	if body.Admin_id == 0 {
		tx.Rollback()
		c.JSON(http.StatusOK, &models.Response{
			Status:  false,
			Message: "Admin id is required!",
			Data:    nil,
		})
		return
	}
	if body.Super_admin_id == 0 {
		tx.Rollback()
		c.JSON(http.StatusOK, &models.Response{
			Status:  false,
			Message: "Super admin id is required!",
			Data:    nil,
		})
	}

	admin.Verified = "true"
	admin.Active = "true"
	if err := tx.Model(&admin).
		Select("verified",
			"active").
		Where("id=?", body.Admin_id).
		Updates(
			map[string]interface{}{
				"verified": admin.Verified,
				"active":   admin.Active,
			}).Error; err != nil {
		tx.Rollback()
		c.JSON(http.StatusBadRequest, &models.Response{
			Status:  false,
			Message: err.Error(),
			Data:    nil,
		})
		return
	}

	if err := tx.Table("admin_registration").
		Select("STATUS").
		Where("admin_id=? AND super_admin_id=?", body.Admin_id, body.Super_admin_id).
		Updates(
			map[string]interface{}{
				"STATUS": "Y",
			}).Error; err != nil {
		tx.Rollback()
		c.JSON(http.StatusBadRequest, &models.Response{
			Status:  false,
			Message: "Error di sini " + err.Error(),
			Data:    nil,
		})
		return
	}

	tx.Commit()
	c.JSON(http.StatusOK, &models.Response{
		Status:  true,
		Message: "Approval succes!",
		Data:    nil,
	})
}
