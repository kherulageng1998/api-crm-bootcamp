package admin

import (
	"api-crm/config"
	"api-crm/models"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	"golang.org/x/crypto/bcrypt"
)

func Login(c *gin.Context) {
	type Body struct {
		Username string `json:"username" binding:"required"`
		Password string `json:"password" binding:"required"`
	}
	var body Body
	if err := c.ShouldBindJSON(&body); err != nil {
		c.JSON(http.StatusBadRequest, &models.Response{
			Status:  false,
			Message: "Failed read body!",
			Data:    nil,
		})
		return
	}

	var user models.Admin
	var count int64
	if err := config.DB.Debug().
		Table("actors").
		Select("id", "username", "password").
		Where("username = ? AND active = true AND verified = true", body.Username).
		Find(&user).
		Count(&count).Error; err != nil {
		c.JSON(http.StatusBadRequest, &models.Response{
			Status:  false,
			Message: "Invalid username!",
			Data:    nil,
		})
		return
	}

	if count < 1 {
		c.JSON(http.StatusBadRequest, &models.Response{
			Status:  false,
			Message: "Invalid username!",
			Data:    nil,
		})
		return
	}
	pass, _ := HashPassword(user.Password)
	if verifyCredentials(body.Password, pass) {
	} else {
		c.JSON(401, gin.H{"error": "Kredensial tidak valid", "input": body.Password, "db": user.Password})
		return
	}

	timeExp := time.Now().Add(time.Hour * 24 * 7).Unix()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub":  user.Id,
		"name": user.Username,
		"exp":  timeExp,
	})

	tokenString, err := token.SignedString([]byte(os.Getenv("SECRET")))
	if err != nil {
		c.JSON(http.StatusBadRequest, &models.Response{
			Status:  false,
			Message: "Failed generate token!",
			Data:    nil,
		})
		return
	}

	c.SetSameSite(http.SameSiteLaxMode)
	c.SetCookie(
		"Authorization",
		tokenString,
		int(timeExp),
		"",
		"",
		false,
		true,
	)
	c.JSON(http.StatusOK, &models.Response{
		Status:  true,
		Message: "Login successfully!",
		Data:    user,
		Token:   tokenString,
	})
}

// Fungsi untuk memverifikasi kredensial pengguna
func verifyCredentials(inputPassword, storedPassword string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(storedPassword), []byte(inputPassword))
	return err == nil
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}
